import { Box, Button, Flex, Link } from "@chakra-ui/react";
import React from "react";
import NextLink from "next/link";
import { useCurrentUserQuery, useLogoutMutation } from "../generated/graphql";

interface NavBarProps {}

export const NavBar: React.FC<NavBarProps> = ({}) => {
  const [{ data, fetching }] = useCurrentUserQuery();
  const [{ fetching: logoutFetching }, logout] = useLogoutMutation();

  return (
    <Flex p={4} ml={"auto"} bg="tomato" position="sticky" top={0} zIndex={9999}>
      <Box ml={"auto"}>
        {!fetching && !data?.currentUser && (
          <>
            <NextLink href="/login">
              <Link mr={2}>Login</Link>
            </NextLink>
            <NextLink href="/register">
              <Link>Register</Link>
            </NextLink>
          </>
        )}
        {!fetching && data?.currentUser && (
          <Flex>
            <Box mr={2}>{data.currentUser.username}</Box>
            <Button
              onClick={() => {
                logout();
              }}
              variant="link"
              isLoading={logoutFetching}
            >
              Logout
            </Button>
          </Flex>
        )}
      </Box>
    </Flex>
  );
};
